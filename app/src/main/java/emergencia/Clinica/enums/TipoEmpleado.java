package emergencia.clinica.enums;

public enum TipoEmpleado {
   
    ADMINISTRATIVO,
    CHOFER,
    ENFERMERO,
    MEDICO

}
