package emergencia.clinica;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;

import emergencia.clinica.enums.TipoEmpleado;

public class Empleado extends Persona {

    protected double sueldo;
    protected LocalDate fecha_ingreso = LocalDate.now();
    protected TipoEmpleado tipoEmpleado;

    public Empleado(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, 
    String email, String telefono,Domicilio domicilio, double sueldo, LocalDate fecha_ingreso, TipoEmpleado tipoEmpleado) {
         
        super(dni, cuit, nombre, apellido, fecha_nacimiento, email, telefono,domicilio);
        
        this.sueldo = sueldo;
        this.fecha_ingreso = fecha_ingreso;
        this.tipoEmpleado = tipoEmpleado;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public LocalDate getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(LocalDate fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }
    
    public TipoEmpleado getTipoEmpleado() {
        return tipoEmpleado;
    }

    public void setTipoEmpleado(TipoEmpleado tipoEmpleado) {
        this.tipoEmpleado = tipoEmpleado;
    }

    //METODOS PERSONALIZADOS

    public int calcularEdadEmpleado(){
        try{
        LocalDate fechaActual=LocalDate.now();
        LocalDate fechaNacimiento=getFecha_nacimiento();
        Period edad=fechaNacimiento.until(fechaActual);
        return edad.getYears();
        }catch(DateTimeParseException ex){
            System.out.println("HA INGRESADO UNA FECHA INVALIDA");
        }
        return 0;
    }

    public int calcularAntiguedadEmpleado(){
        try{
        LocalDate fechaActual=LocalDate.now();
        LocalDate fechaIngreso=getFecha_ingreso();
        Period antiguedad=fechaIngreso.until(fechaActual);
        return antiguedad.getYears();
        }catch(DateTimeParseException ex){
            System.out.println("HA INGRESADO UNA FECHA INVALIDA");
        } 
        return 0;
    }

    public double aumentoSueldoEmpleado(int porcentaje){
        double aumento;
        if (porcentaje<=0){
            System.out.println("NO HAY AUMENTO DE SUELDO");
            aumento=0;
        }
        else{
            aumento=(getSueldo()*((double)porcentaje/100));
        }
        return aumento;
    }
}