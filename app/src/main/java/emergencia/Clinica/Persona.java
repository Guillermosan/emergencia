package emergencia.clinica;

import java.time.LocalDate;

public abstract class Persona {
    protected String dni;
    protected String cuit;
    protected String nombre;
    protected String apellido;
    protected LocalDate fecha_nacimiento = LocalDate.now(); 
    protected String email;
    protected String telefono;
    protected Domicilio domicilio;

    public Persona(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, 
    String email, String telefono,Domicilio domicilio) {
        this.dni = dni;
        this.cuit = cuit;
        this.nombre = nombre;
        this.apellido = apellido;
        this.fecha_nacimiento = fecha_nacimiento;
        this.email = email;
        this.telefono = telefono;
        this.domicilio = domicilio;
    }
    public String getDni() {
        return dni;
    }
    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCuit() {
        return cuit;
    }
    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }
    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}