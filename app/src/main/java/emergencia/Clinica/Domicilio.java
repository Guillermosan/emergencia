package emergencia.clinica;

public class Domicilio {
    private String calle;
    private String numero;
    private String piso;
    private String departamento;
    private String cod_postal;
    private String provincia;
    private String localidad;
    
    public Domicilio(String calle, String numero, String piso, String departamento, String cod_postal, String provincia,
            String localidad) {
        
        this.calle = calle;
        this.numero = numero;
        this.piso = piso;
        this.departamento = departamento;
        this.cod_postal = cod_postal;
        this.provincia = provincia;
        this.localidad = localidad;
    }

    public String getCalle() {
        return calle;
    }
    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }
    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getPiso() {
        return piso;
    }
    public void setPiso(String piso) {
        this.piso = piso;
    }

    public String getDepartamento() {
        return departamento;
    }
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getCod_postal() {
        return cod_postal;
    }
    public void setCod_postal(String cod_postal) {
        this.cod_postal = cod_postal;
    }

    public String getProvincia() {
        return provincia;
    }
    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getLocalidad() {
        return localidad;
    }
    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }
}