package emergencia.gestionAsistenciasMedicas.registros;

import java.util.ArrayList;
import java.util.List;
import emergencia.exceptionPkg.AfiliadoExcepcion;
import emergencia.gestionAsistenciasMedicas.Diagnostico;

public class ListaDiagnostico {
    public List<Diagnostico> diagnosticoList = new ArrayList<Diagnostico>();

    public ListaDiagnostico(List<Diagnostico> diagnosticoList) {
        this.diagnosticoList = diagnosticoList;
    }

    public List<Diagnostico> getDiagnosticosList() {
        return diagnosticoList;
    }

    public void setDiagnosticosList(List<Diagnostico> diagnosticoList) {
        this.diagnosticoList = diagnosticoList;
    }

    public void agregarDiagnostico(Diagnostico diagnostico) {
        diagnosticoList.add(diagnostico);
    }

    //Obtener todos los diagnosticos de un afiliado
    public ArrayList<Diagnostico> getDiagnosticoAfiliado(String dni) throws AfiliadoExcepcion {
        ArrayList<Diagnostico> afiliadoDiag = new ArrayList<Diagnostico>();

        for (Diagnostico diagnostico : diagnosticoList) {
            if (diagnostico.getAfiliado().getDni().equals(dni)) {
                afiliadoDiag.add(diagnostico);
            }
            throw new AfiliadoExcepcion("No se encontró ningún afiliado con el DNI");
        }
        return afiliadoDiag;
    }
}