package emergencia.gestionAsistenciasMedicas;

import java.time.LocalDate;
import emergencia.gestionAfiliados.Afiliado;
import emergencia.gestionAsistenciasMedicas.enums.Prioridad;
import emergencia.gestionMoviles.Movil;

public class SolicitudMedica {
    private String descripcion;
    private Prioridad prioridad;
    private LocalDate fechaAsistencia;
    private Diagnostico diagnostico;
    private Afiliado afiliado;
    private Movil ambulancia;

    public SolicitudMedica(String descripcion, Prioridad prioridad, LocalDate fechaAsistencia, Diagnostico diagnostico,
        Afiliado afiliado, Movil ambulancia) {
        this.descripcion = descripcion;
        this.prioridad = prioridad;
        this.fechaAsistencia = fechaAsistencia;
        this.diagnostico = diagnostico;
        this.afiliado = afiliado;
        this.ambulancia = ambulancia;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Prioridad getPrioridad() {
        return prioridad;
    }
    public void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }

    public LocalDate getFechaAsistencia() {
        return fechaAsistencia;
    }
    public void setFechaAsistencia(LocalDate fechaAsistencia) {
        this.fechaAsistencia = fechaAsistencia;
    }

    public Diagnostico getDiagnostico() {
        return diagnostico;
    }
    public void setDiagnostico(Diagnostico diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }
    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public Movil getAmbulancia() {
        return ambulancia;
    }
    public void setAmbulancia(Movil ambulancia) {
        this.ambulancia = ambulancia;
    }
}