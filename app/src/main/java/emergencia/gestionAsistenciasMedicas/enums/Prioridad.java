package emergencia.gestionAsistenciasMedicas.enums;

public enum Prioridad {
    ATENCION_INMEDIATA, EMERGENCIA, URGENCIA, PRIORITARIO, NO_URGENTE;
}
