package emergencia.gestionAsistenciasMedicas;

import emergencia.gestionAfiliados.Afiliado;
import emergencia.gestionEmpleados.Medico;

public class Diagnostico {
    private int NumeroDiagnostico;
    private String diagnostico;
    private String descripcion;
    private Afiliado afiliado;
    private Medico Medico;
    private SolicitudMedica solicitudMedica;
    
    public Diagnostico(int NumeroDiagnostico, String diagnostico, String descripcion, Afiliado afiliado, 
    Medico medico, SolicitudMedica solicitudMedica) {
        this.NumeroDiagnostico = NumeroDiagnostico;
        this.diagnostico = diagnostico;
        this.descripcion = descripcion;
        this.afiliado = afiliado;
        this.Medico = medico;
        this.solicitudMedica = solicitudMedica;
    }

    public int getNumeroDiagnostico() {
        return NumeroDiagnostico;
    }

    public void setNumeroDiagnostico(int numeroDiagnostico) {
        NumeroDiagnostico = numeroDiagnostico;
    }

    public String getDiagnostico() {
        return diagnostico;
    }
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }
    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public Medico getMedico() {
        return Medico;
    }
    public void setMedico(Medico medico) {
        Medico = medico;
    }

    public SolicitudMedica getSolicitudMedica() {
        return solicitudMedica;
    }
    public void setSolicitudMedica(SolicitudMedica solicitudMedica) {
        this.solicitudMedica = solicitudMedica;
    }
}