package emergencia.gestionAfiliados.registroAfiliados;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import emergencia.clinica.Existencia;
import emergencia.exceptionPkg.NoAccesable;
import emergencia.exceptionPkg.NoEncontrado;
import emergencia.exceptionPkg.YaExiste;
import emergencia.gestionAfiliados.Afiliado;

public class ListaAfiliados {

    public ArrayList<Afiliado> afiliadosList = new ArrayList<Afiliado>();
 
    public void crearArrayList(){
       afiliadosList = new ArrayList<Afiliado>();
    }

    public ArrayList<Afiliado> getAfiliadosList() {
             return afiliadosList;
    }

    public void setAfiliadosList(ArrayList<Afiliado> afiliadosList) {
        this.afiliadosList = afiliadosList;
    }

    //VER EXCEPCIONES(TRY CATCH) + TEST! + METODOS PROPIOS
    //METODO INGRESAR AFILIADO
    public void altaAfiliado(Afiliado afiliado){
        if(afiliado.getDni()==null || afiliado.getNombre()==null || afiliado.getApellido()==null){
            throw new NullPointerException("No se puede agregar afiliado sin los campos obligatorios (DNI,NOMBRE,APELLIDO)");
        }
        afiliadosList.add(new Afiliado(null, null, null, null, null, null, null, null, null, null, false));
    }
    //METODO MODIFICAR AFILIADO
    public void modificarAfiliado(Afiliado afiliado){
        if(afiliado.getDni()==null || afiliado.getNombre()==null || afiliado.getApellido()==null){
            throw new NullPointerException("No se puede modificar afiliado sin los campos obligatorios (DNI,NOMBRE,APELLIDO)");
        }
        afiliadosList.set(0, afiliado);
    }
    //METODO BORRAR AFILIADO
    public void bajaAfiliado(Afiliado afiliado){
        if(!afiliadosList.contains(afiliado)){
            throw new NoSuchElementException("El afiliado no existe");
        }
        afiliadosList.remove(afiliado);
    }

    //BUSCAR SI EXISTE UN AFILIADO (retorna un objeto con booleano y posición del objeto)
    public Existencia existe(String dni) throws NoAccesable{
        if(afiliadosList == null){
            throw new NoAccesable("No se ha creado la lista");
        }
        Existencia existe = new Existencia(false, 0);
        int i = 0;
        //Recorremos en busca de coincidencias
        for(Afiliado afiliado:afiliadosList){
            if(afiliado.getDni() == dni){
                existe.setExiste(true);
                existe.setIndex(i);
                return existe;
            }
            i++;
        }
        return existe;
    }

    // CRUD
    //AGREGAR
    public void insertarAfiliado(Afiliado afiliado)throws YaExiste,NoAccesable {
        
        for (int i = 0 ; i < afiliadosList.size(); i++){
            if(afiliadosList.get(i).getDni().equals(afiliado.getDni())){
                throw new YaExiste("Ya existe este afiliado"); 
            }

        }    afiliadosList.add(afiliado);    
    }
     
    //LEER  
    public String listarAfiliados() throws NoAccesable,NoEncontrado{
        String vista="";
        for(int i=0; i < afiliadosList.size(); i++){
            vista += afiliadosList.get(i).toString();
            } return vista;
        }
     
    //MODIFICAR
    public boolean modificarAfiliados(Afiliado afiliado) throws NoEncontrado, NoAccesable{
    
        boolean encontrado = false;
            for (int i=0; i < afiliadosList.size();i++){
                if(afiliadosList.get(i).getCuit().equals(afiliado.getCuit())){
                    encontrado = true;
                    afiliadosList.get(i).setNombre(afiliado.getNombre());
                }
            }return encontrado;
        }
    
    //BORRAR
    public boolean eliminarAfiliado(String cuit) throws NoEncontrado,NoAccesable{
    
        boolean encontrado = false;
            for (int i=0; i < afiliadosList.size();i++){
                if(afiliadosList.get(i).getCuit().equals(cuit)){
                    encontrado = true;
                    afiliadosList.remove(i);
                }
            }return encontrado;
        }

    @Override
    public String toString() {
        return "Afiliado { " + afiliadosList + " }"+"\n";
    }
}