package emergencia.gestionAfiliados;

import java.time.LocalDate;
import emergencia.clinica.Domicilio;
import emergencia.clinica.Persona;

public class Familiar extends Persona {
    private LocalDate fecha_afiliacion = LocalDate.now();
    private String parentezco;
    private Afiliado afiliado;

    public Familiar(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, 
    String email, String telefono,Domicilio domicilio, LocalDate fecha_afiliacion, String parentezco, Afiliado afiliado) {
        
        super(dni, cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio);
        
        this.parentezco = parentezco;
        this.afiliado = afiliado;
    }

    public LocalDate getFecha_afiliacion() {
        return fecha_afiliacion;
    }
    public void setFecha_afiliacion(LocalDate fecha_afiliacion) {
        this.fecha_afiliacion = fecha_afiliacion;
    }

    public String getParentezco() {
        return parentezco;
    }
    public void setParentezco(String parentezco) {
        this.parentezco = parentezco;
    }
     
    public Afiliado getAfiliado() {
        return afiliado;
    }
    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }
}