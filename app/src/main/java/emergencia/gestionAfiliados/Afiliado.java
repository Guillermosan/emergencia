package emergencia.gestionAfiliados;

import java.time.LocalDate;

import emergencia.clinica.Domicilio;
import emergencia.clinica.Persona;
import emergencia.gestionAfiliados.enums.Plan;
 
public class Afiliado extends Persona {
    private Plan plan;
    private LocalDate fecha_afiliacion = LocalDate.now();
    private LocalDate fecha_ult_pago = LocalDate.now();
    private boolean estadoAfiliado=true;

    public Afiliado(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, 
    String email, String telefono,Domicilio domicilio, LocalDate fecha_afiliacion, LocalDate fecha_ult_pago,
    boolean estadoAfiliado) {
        
        super(dni, cuit, nombre, apellido, fecha_nacimiento, email, telefono,domicilio);
        
        this.fecha_afiliacion = fecha_afiliacion;
        this.fecha_ult_pago = fecha_ult_pago;
        this.estadoAfiliado=estadoAfiliado;
    }

    public LocalDate getFecha_afiliacion() {
        return fecha_afiliacion;
    }

    public void setFecha_afiliacion(LocalDate fecha_afiliacion) {
        this.fecha_afiliacion = fecha_afiliacion;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public LocalDate getFechaUltimoPago() {
        return fecha_ult_pago;
    }

    public void setFechaUltimoPago(LocalDate fecha_ult_pago) {
        this.fecha_ult_pago = fecha_ult_pago;
    }

    @Override
    public String toString() {
        return "Afiliado{" + "fecha_afiliacion=" + fecha_afiliacion + ", fechaUltimoPago=" + fecha_ult_pago + ", plan=" + plan + '}';
    }

    public boolean isEstadoAfiliado() {
        return estadoAfiliado;
    }

    public void setEstadoAfiliado(boolean estadoAfiliado) {
        this.estadoAfiliado = estadoAfiliado;
    }

    //METODOS PROPIOS
    public String estadoAfiliado(boolean estadoAfiliado){
        String estado;
        if (estadoAfiliado==false){
            estado="EL AFILIADO ESTA INACTIVO";
        }
        else{
            estado="EL AFILIADO ESTA ACTIVO";
        }
        return estado;
    }
}