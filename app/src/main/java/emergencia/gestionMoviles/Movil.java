package emergencia.gestionMoviles;

import emergencia.gestionEmpleados.Chofer;
import emergencia.gestionEmpleados.Enfermero;
import emergencia.gestionEmpleados.Medico;

public class Movil {
    private String marca;
    private int anio;
    private String modelo;
    private Medico medico_movil;
    private Enfermero enfermero_movil;
    private Chofer chofer_movil;
    private boolean estadoMovil;

    public Movil(String marca, int anio, String modelo, Medico medico_movil
    , Enfermero enfermero_movil, Chofer chofer_movil, boolean estadoMovil) {
        this.marca = marca;
        this.anio = anio;
        this.modelo = modelo;
        this.medico_movil = medico_movil;
        this.enfermero_movil = enfermero_movil;
        this.chofer_movil = chofer_movil;
        this.estadoMovil = estadoMovil;
    }

    public String getMarca() {
        return marca;
    }
    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAnio() {
        return anio;
    }
    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getModelo() {
        return modelo;
    }
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void SetMedico(Medico medico_movil){
        this.medico_movil = medico_movil;
    }

    public Medico getMedico(){
        return medico_movil;
    }

    public void setEnfermero(Enfermero enfermero_movil){
        this.enfermero_movil = enfermero_movil;
    }

    public Enfermero getEnfermero(){
        return enfermero_movil;
    }

    public void setChofer(Chofer chofer_movil){
        this.chofer_movil = chofer_movil;
    }

    public Chofer getChofer(){
        return chofer_movil;
    }

    public boolean isEstadoMovil() {
        return estadoMovil;
    }

    public void setEstadoMovil(boolean estadoMovil) {
        this.estadoMovil = estadoMovil;
    }

    //METODOS PROPIOS
    public String estadoMovil(boolean estadoMovil){
        String estado;
        if (estadoMovil==false){
            estado="EL MOVIL NO ESTA DISPONIBLE";
        }
        else{
            estado="EL MOVIL ESTA DISPONIBLE";
        }
        return estado;
    }
}