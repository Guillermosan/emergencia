package emergencia.gestionMoviles.registroMoviles;

import java.util.ArrayList;
import java.util.List;
import emergencia.gestionMoviles.Movil;

public class ListaMoviles {
    List<Movil> movilesList=new ArrayList<Movil>();

    public ListaMoviles(List<Movil> movilesList) {
        this.movilesList = movilesList;
    }

    public List<Movil> getMovilesList() {
        return movilesList;
    }

    public void setMovilesList(List<Movil> movilesList) {
        this.movilesList = movilesList;
    }
}