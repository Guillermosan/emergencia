package emergencia.exceptionPkg; 

public class NoAccesable extends Exception {
        
    public NoAccesable(String mensaje) {
        super(mensaje);
    }
}