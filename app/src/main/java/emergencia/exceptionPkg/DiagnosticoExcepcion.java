package emergencia.exceptionPkg; 

public class DiagnosticoExcepcion extends Exception {

    public DiagnosticoExcepcion(String mensaje) {
        super(mensaje);
    }
}