package emergencia.exceptionPkg; 

public class YaExiste extends Exception {
     
    public YaExiste(String mensaje){
        super(mensaje);
    }
}