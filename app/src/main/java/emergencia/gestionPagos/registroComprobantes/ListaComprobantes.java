package emergencia.gestionPagos.registroComprobantes;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import emergencia.gestionPagos.Comprobante;

public class ListaComprobantes {
    List<Comprobante> comprobantesList=new ArrayList<Comprobante>();

    public List<Comprobante> getComprobantesList() {
        return comprobantesList;
    }

    public void setComprobantesList(List<Comprobante> comprobantesList) {
        this.comprobantesList = comprobantesList;
    }

    public ListaComprobantes(List<Comprobante> comprobantesList) {
        this.comprobantesList = comprobantesList;
    }

    //VER EXCEPCIONES(TRY CATCH) + TEST! + METODOS PROPIOS
    //METODO INGRESAR COMPROBANTE
    public void ingresarComprobante(Comprobante comprobante){
        if(comprobante.getTicketPago()==null || comprobante.getImportePago()==0){
            throw new NullPointerException("No se puede agregar comprobante sin los campos obligatorios (Nº TICKET,IMPORTE)");
        }
        comprobantesList.add(new Comprobante(null, null, 0, null, null, null));
    }
    //METODO MODIFICAR COMPROBANTE
    public void modificarComprobante(Comprobante comprobante){
        if(comprobante.getTicketPago()==null || comprobante.getImportePago()==0){
            throw new NullPointerException("No se puede modificar comprobante sin los campos obligatorios (Nº TICKET,IMPORTE)");
        }
        comprobantesList.set(0, comprobante);
    }
    //METODO BORRAR COMPROBANTE
    public void eliminarComprobante(Comprobante comprobante){
        if(!comprobantesList.contains(comprobante)){
            throw new NoSuchElementException("El comprobante no existe");
        }
        comprobantesList.remove(comprobante);
    }
}