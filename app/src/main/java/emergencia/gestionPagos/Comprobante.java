package emergencia.gestionPagos;

import java.time.LocalDate;
import emergencia.gestionAfiliados.Afiliado;

public class Comprobante {
    
    protected String ticketPago;
    protected String medioPago;
    protected double importePago;
    protected LocalDate fecha_pago = LocalDate.now();
    protected Boolean anulado = false;
    protected Afiliado afiliado;

     public Comprobante(String ticketPago, String medioPago, double importePago, LocalDate fecha_pago,
        Boolean anulado, Afiliado afiliado) {
        this.ticketPago = ticketPago;
        this.medioPago = medioPago;
        this.importePago = importePago;
        this.fecha_pago = fecha_pago;
        this.anulado = anulado;
        this.afiliado = afiliado;
    }

    public String getTicketPago() {
        return ticketPago;
    }

    public void setTicketPago(String ticketPago) {
        this.ticketPago = ticketPago;
    }

    public Afiliado getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public LocalDate getFecha_pago() {
        return fecha_pago;
    }

    public void setFecha_pago(LocalDate fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public double getImportePago() {
        return importePago;
    }

    public void setImportePago(double importePago) {
        this.importePago = importePago;
    }

    public Boolean getAnulado() {
        return anulado;
    }

    public void setAnulado(Boolean anulado) {
        this.anulado = anulado;
    }

    //METODOS PROPIOS
    public double descuentoComprobante(int porcentaje){
        double descuento;
        if (porcentaje<=0){
            System.out.println("NO HAY DESCUENTO");
            descuento=0;
        }
        else{
            descuento=(getImportePago()*((double)porcentaje/100));
        }
        return descuento;
    }

    //VER ITEMS DEL COMPROBANTE!
}