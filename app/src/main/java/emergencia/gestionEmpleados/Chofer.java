package emergencia.gestionEmpleados;

import java.time.LocalDate;
import emergencia.clinica.Domicilio;
import emergencia.clinica.Empleado;
import emergencia.clinica.enums.TipoEmpleado;

public class Chofer extends Empleado {

    private String tipoLicencia;

    public Chofer(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, String email,
            String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso, TipoEmpleado tipoEmpleado,
            String tipoLicencia) {
        super(dni, cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio, sueldo, fecha_ingreso,
                tipoEmpleado);
        this.tipoLicencia = tipoLicencia;
    }

    public String getTipoLicencia() {
        return tipoLicencia;
    }
    public void setTipoLicencia(String tipoLicencia) {
        this.tipoLicencia = tipoLicencia;
    }
}