package emergencia.gestionEmpleados;

import java.time.LocalDate;
import emergencia.clinica.Domicilio;
import emergencia.clinica.Empleado;
import emergencia.clinica.enums.TipoEmpleado;

public class Medico extends Empleado {
    
    private String especialidad;

    public Medico(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento, String email,
            String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso, TipoEmpleado tipoEmpleado,
            String especialidad) {
        super(dni, cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio, sueldo, fecha_ingreso,
                tipoEmpleado);
        this.especialidad = especialidad;
    }

    public String getEspecialidad() {
        return especialidad;
    }
    
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
}