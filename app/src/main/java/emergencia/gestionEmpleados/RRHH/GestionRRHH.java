package emergencia.gestionEmpleados.RRHH;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import emergencia.clinica.Empleado;

public class GestionRRHH {
    List<Empleado> listaEmpleados=new ArrayList<Empleado>();

    public GestionRRHH(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

    public List<Empleado> getListaEmpleados() {
        return listaEmpleados;
    }
    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }
    //VER EXCEPCIONES(TRY CATCH) + TEST! + METODOS PROPIOS
    //METODO INGRESAR EMPLEADO
    public void altaEmpleado(Empleado empleado){
        if(empleado.getDni()==null || empleado.getNombre()==null || empleado.getApellido()==null){
            throw new NullPointerException("No se puede agregar empleado sin los campos obligatorios (DNI,NOMBRE,APELLIDO)");
        }
        listaEmpleados.add(new Empleado(null, null, null, null, null, null, null, null, 0, null, null));
    }
    //METODO MODIFICAR EMPLEADO
    public void modificarEmpleado(Empleado empleado){
        if(empleado.getDni()==null || empleado.getNombre()==null || empleado.getApellido()==null){
            throw new NullPointerException("No se puede modificar empleado sin los campos obligatorios (DNI,NOMBRE,APELLIDO)");
        }
        listaEmpleados.set(0, empleado);
    }
    //METODO BORRAR EMPLEADO
    public void bajaEmpleado(Empleado empleado){
        if(!listaEmpleados.contains(empleado)){
            throw new NoSuchElementException("El empleado no existe");
        }
        listaEmpleados.remove(empleado);
    }
}