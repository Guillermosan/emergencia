package emergencia.gestionEmpleados;

import java.time.LocalDate;
import emergencia.clinica.Domicilio;
import emergencia.clinica.Empleado;
import emergencia.clinica.enums.TipoEmpleado;

public class Administrativo extends Empleado {
    
    private String categoria;
    
    public Administrativo(String dni, String cuit, String nombre, String apellido, LocalDate fecha_nacimiento,
            String email, String telefono, Domicilio domicilio, int sueldo, LocalDate fecha_ingreso,
            TipoEmpleado tipoEmpleado, String categoria) {
        
        super(dni, cuit, nombre, apellido, fecha_nacimiento, email, telefono, domicilio, sueldo, fecha_ingreso, tipoEmpleado);
        
        this.categoria = categoria;
    }
    
    public String getCategoria() {
        return categoria;
    }
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}