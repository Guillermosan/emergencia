package gestionPagosTest;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import emergencia.gestionPagos.Comprobante;

public class DescuentoComprobanteTest {
        @Test public void descuentoComprobante() {
        Comprobante comprobante=new Comprobante(null, null, 0, null, null, null);
        comprobante.setImportePago(1000);
        assertEquals(100,comprobante.descuentoComprobante(10),10);
    }
}