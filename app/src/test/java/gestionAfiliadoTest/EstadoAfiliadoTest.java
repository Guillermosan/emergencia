package gestionAfiliadoTest;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import emergencia.gestionAfiliados.Afiliado;

public class EstadoAfiliadoTest {
    @Test public void estadoAfiliado() {
        Afiliado afiliado=new Afiliado(null, null, null, null, null, null, null, null, null, null, false);
        assertEquals("EL AFILIADO ESTA ACTIVO",afiliado.estadoAfiliado(true));
    }
}