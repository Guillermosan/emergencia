package gestionMovilesTest;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import emergencia.gestionMoviles.Movil;

public class EstadoMovilTest {
    @Test public void estadoMovil() {
        Movil movil=new Movil(null, 0, null, null, null, null, false);
        assertEquals("EL MOVIL ESTA DISPONIBLE",movil.estadoMovil(true));
    }
}