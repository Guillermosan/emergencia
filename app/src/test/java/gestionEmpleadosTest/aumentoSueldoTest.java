package gestionEmpleadosTest;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import emergencia.clinica.Empleado;

public class AumentoSueldoTest {
    @Test public void aumentoSueldo() {
        Empleado empleado=new Empleado(null, null, null, null, null, null, null, null, 0, null, null);
        empleado.setSueldo(1000);
        assertEquals(100,empleado.aumentoSueldoEmpleado(10),10);
    }
}