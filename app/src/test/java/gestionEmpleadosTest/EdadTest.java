package gestionEmpleadosTest;

import org.junit.Test;
import emergencia.clinica.Empleado;
import static org.junit.Assert.assertEquals;
import java.time.LocalDate;

public class EdadTest {
    @Test
    public void testEdad() {
        Empleado empleado = new Empleado( null, null, null, null, null, null, null, null, 0, null, null);
        empleado.setFecha_nacimiento(LocalDate.of(1986,01,01));
        assertEquals(37,empleado.calcularEdadEmpleado());
    }
}