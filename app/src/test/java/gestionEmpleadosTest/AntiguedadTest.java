package gestionEmpleadosTest;

import static org.junit.Assert.assertEquals;
import java.time.LocalDate;
import emergencia.clinica.Empleado;
import org.junit.Test;

public class AntiguedadTest {
    @Test
    public void testAntiguedad() {
        Empleado empleado = new Empleado(null, null, null, null, null, null, null, null, 0, null, null);
        empleado.setFecha_ingreso(LocalDate.of(2000,01,01));
        assertEquals(23,empleado.calcularAntiguedadEmpleado());
    }
}